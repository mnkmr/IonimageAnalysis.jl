module IonimageAnalysis

export
    analyze,
    pixel_to_speed,
    speed_to_energy,
    pixel_to_energy,
    energy_to_speed,
    crop,
    shear,
    average,
    smooth,
    angularfit,
    radialdist,
    speeddist,
    energydist,
    anisotropy,
    SlicedImage,
    angulardist,
    PBasex,
    PBasexImage,
    savepbasex,
    loadpbasex

using IonimageAnalysisCore:
    pixel_to_speed,
    speed_to_energy,
    pixel_to_energy,
    energy_to_speed,
    crop,
    shear,
    average,
    smooth,
    angularfit,
    radialdist,
    speeddist,
    energydist,
    anisotropy,
    circumscribedsquare,
    maxradius,
    cornerpos

using SlicedIonimageAnalysis:
    SlicedImage,
    angulardist

using PBasexInversion:
    PBasex,
    PBasexImage,
    savepbasex,
    loadpbasex

using PBasexCache: PBasexCache
using PyPlot: PyPlot, Figure, figure, colorbar

FIGURENUMBER = 10000
FIGURESHOWN = false
LASTFIGURE = :empty


"""
    analyze(::Type{SlicedImage}, image::AbstractMatrix, center; plot::Bool=true, kwargs...)
    analyze(::Type{SlicedImage}, image::AbstractMatrix; plot::Bool=true, kwargs...)

Plot the radial angular distributions of `image` and return an instance of
`SlicedImage`. `center` is the center coordinate of the image; if it is omitted
the center coordinate is estimated by weighted average of all signals.

The plot will be suppressed if `plot` is `false`. Other kwargs will be passed
to the constructor of [`SlicedImage`](@ref).
"""
function analyze(::Type{SlicedImage}, image::AbstractMatrix, center; plot::Bool=true, kwargs...)
    s = SlicedImage(image, center; kwargs...)
    plot && plot_summary(s)
    return s
end
function analyze(::Type{SlicedImage}, image::AbstractMatrix; kwargs...)
    yc, xc = guesscenter(image)
    return analyze(SlicedImage, image, (yc, xc); kwargs...)
end

"""
    analyze(pbasex::PBasex, image::AbstractMatrix, center; plot::Bool=true)
    analyze(pbasex::PBasex, image::AbstractMatrix; kwargs...)

Plot the radial angular distributions of `image` and return an instance of
`PBasexImage`. `center` is the center coordinate of the image; if it is omitted
the center coordinate is estimated by weighted average of all signals.

The plot will be suppressed if `plot` is `false`. Other kwargs will be passed
to the call of [`PBasex`](@ref) instance.
"""
function analyze(pbasex::PBasex, image::AbstractMatrix, center; plot::Bool=true, kwargs...)
    inv = pbasex(image, center; kwargs...)
    plot && plot_summary(inv, image, center)
    return inv
end
function analyze(pbasex::PBasex, image::AbstractMatrix; kwargs...)
    len = pbasex.len
    if size(image, 1) < len || size(image, 2) < len
        ArgumentError("The basis set size ($(len)×$(len)) should be equal or smaller than the image size ($(size(image))).") |> throw
    end

    if size(image, 1) == len && size(image, 2) == len
        yc = xc = (len + 1)/2
        return analyze(pbasex, image, (yc, xc))
    end

    rmax = (len - 1)/2
    yc0, xc0 = guesscenter(image)
    yc = min(max(rmax + 1, yc0), size(image, 1) - rmax)
    xc = min(max(rmax + 1, xc0), size(image, 2) - rmax)
    return analyze(pbasex, image, (yc, xc))
end


const COLORMAP = Ref{String}("turbo")

function plot_summary(s::SlicedImage)
    fig = initialize_figure(:sliced, 8, 3)
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)

    img = plot_image_pane(ax1, s.arr, s.center, last(s.r))
    colorbar(img, ax=ax1)
    ax2, ax3 = plot_distribution_pane(ax2, s)

    fig.tight_layout()
    fig.canvas.draw()
    return fig, ax1, ax2, ax3
end

function plot_summary(inv::PBasexImage, image, center)
    fig = initialize_figure(:inverted, 11, 3)
    ax1 = fig.add_subplot(1, 3, 1)
    ax2 = fig.add_subplot(1, 3, 2)
    ax3 = fig.add_subplot(1, 3, 3)

    orig = plot_image_pane(ax1, image, center, (inv.len - 1)/2)
    colorbar(orig, ax=ax1)
    ax1.set_title("original")

    invarr = Array(inv)
    invvec = reshape(Array(inv), :)
    ignorelen = floor(Int, length(invvec)*0.001)
    vmax = sort(invvec)[end - ignorelen]
    recons = ax2.imshow(invarr, cmap=COLORMAP[], vmin=0.0, vmax=vmax)
    colorbar(recons, ax=ax2)
    ax2.set_title("reconstructed")
    ax3, ax4 = plot_distribution_pane(ax3, inv)

    fig.tight_layout()
    fig.canvas.draw()
    return fig, ax1, ax2, ax3, ax4
end


function initialize_figure(kind, w, h)
    global FIGURESHOWN, FIGURENUMBER, LASTFIGURE

    if isinIJulia()
        fig = figure(FIGURENUMBER, figsize=(w, h))
    else
        if !FIGURESHOWN
            fig = figure(FIGURENUMBER, figsize=(w, h))
            fig.canvas.mpl_connect("close_event", handle_window_close)
            matplotlib_ver = VersionNumber(PyPlot.matplotlib.__version__)
            if matplotlib_ver > v"3.4"
                fig.canvas.manager.set_window_title("IonimageAnalysis")
            else
                fig.canvas.set_window_title("IonimageAnalysis")
            end
            FIGURESHOWN = true
        else
            fig = figure(FIGURENUMBER)
            fig.clf()
            LASTFIGURE != kind && fig.set_size_inches(w, h)
        end
    end
    LASTFIGURE = kind
    return fig
end

isinIJulia() = isdefined(Main, :IJulia) && Main.IJulia.inited

function handle_window_close(args...)
    global LASTFIGURE
    LASTFIGURE = :empty
    return
end


function plot_image_pane(ax, image, center, rmax)
    image, center = circumscribedsquare(image, center, rmax)
    rmax = maxradius(image, center)
    ymin, ymax, xmin, xmax = cornerpos(image, center, rmax)
    pyextent = xmin, xmax, ymax, ymin
    img = ax.imshow(image, aspect="equal", cmap=COLORMAP[], extent=pyextent)
    chair = crosshair(image, center)
    ax.imshow(chair, aspect="equal", extent=pyextent)
    return img
end


function plot_distribution_pane(ax, imagedata)
    red = "tab:red"
    r, I = radialdist(imagedata)
    ax.plot(r, I, "-", color=red, label="Radial distribution")
    ax.set_xlabel("Radius / pixel")
    ax.set_ylabel("Radial distribution", color=red)
    ax.tick_params(axis="y", labelcolor=red)

    blue = "tab:blue"
    r, b, berr, l = anisotropy(imagedata)
    markers = ("o", "x", "^", "s", "+", "v", "1", "*")
    ax′ = ax.twinx()
    for (i, l) in enumerate(l)
        m = markers[(i-1)%length(markers)+1]
        indices = filterindices(x -> !isnan(x) && x <= 0.5, berr[:, i])
        x = r[indices]
        y = b[indices, i]
        yerr = berr[indices, i]
        ax′.errorbar(x, y, yerr, fmt=m, capsize=3, label="β$(l)")
    end
    ax′.set_ylabel("Anisotropy parameters β", color=blue)
    ax′.tick_params(axis="y", labelcolor=blue)
    ax′.set_ylim([-1.2, 2.2])
    ax′.legend()
    return ax, ax′
end


function filterindices(f, itr)
    indices = Int[]
    for (i, x) in enumerate(itr)
        f(x) && push!(indices, i)
    end
    return indices
end


const CROSSHAIR = Dict{String,Any}(
    "color" => (1.0, 1.0, 1.0, 0.5),
    "r" => (0.4, 0.8),
)

function crosshair(image, center; color=nothing)
    chair = zeros(Float64, (size(image, 1), size(image, 2), 4))
    if isnothing(color)
        color = CROSSHAIR["color"]
    end
    foreach(i -> fill!(view(chair, :, :, i), color[i]), 1:3)
    yc, xc = center
    ymax, xmax = size(image)
    offset = maximum(size(image))*0.005/2
    vertline = max(1, round(Int, yc-offset)):min(xmax, round(Int, yc+offset))
    horiline = max(1, round(Int, xc-offset)):min(ymax, round(Int, xc+offset))
    transparency = color[4]
    # vertical and horizontal lines
    chair[horiline, :, 4] .= transparency
    chair[:, vertline, 4] .= transparency
    # circles
    rmax = maxradius(image, center) - offset
    for rc in round.(Int, rmax.*CROSSHAIR["r"])
        for (y, x) in circleindices(image, center, rc, offset)
            chair[y, x, 4] = transparency
        end
    end
    return chair
end


function circleindices(image, center, rc, offset)
    rmin = round(Int, max(1, rc - offset))
    rmax = round(Int, rc + offset)
    indices = Tuple{Int,Int}[]
    for r in rmin:rmax
        _circle!(indices, r)
    end
    unique!(sort!(indices))
    yc, xc = round.(Int, center)
    ymax, xmax = size(image)
    indices = map(t -> (yc + t[1], xc + t[2]), indices)
    indices = filter(t -> (1 ≤ t[1] ≤ ymax) && (1 ≤ t[2] ≤ xmax), indices)
    return indices
end


function _circle!(list, r)
    addpos(list, x, y) = append!(list,
            [( x,  y), (-x,  y), ( x, -y), (-x, -y),
             ( y,  x), (-y,  x), ( y, -x), (-y, -x)])
    x, y = r, 0
    d = 3 - 2r
    while x ≥ y
        addpos(list, x, y)
        if d ≥ 0
            x -= 1
            d -= 4x
        end
        y += 1
        d += 4y + 2
    end
    return
end


"""
    guesscenter(image)

Return an approximate center of the image. The center coordinate is obtained as
the centroid of a binarized image.
"""
function guesscenter(image, thr=nothing)
    if isnothing(thr)
        cmin, cmax = extrema(image)
        thr = (cmax - cmin)*0.1 + cmin
    end
    binary = binarize(image, thr)
    ymax, xmax = size(binary)
    yc, xc = 0, 0
    for x in 1:xmax, y in 1:ymax
        @inbounds binary[y, x] || continue
        yc += y
        xc += x
    end
    return round.(Int, (yc, xc)./sum(binary))
end


"Return a binary image"
function binarize(image, thr=nextfloat(0.0))
    newimg = similar(image, Bool)
    for i in eachindex(image)
        @inbounds newimg[i] = ifelse(image[i] >= thr, true, false)
    end
    return newimg
end


end # module
